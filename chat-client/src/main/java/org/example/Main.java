package org.example;

import com.rabbitmq.client.*;
import org.example.service.UserService;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

public class Main {
    private static Connection conn;
    private static Channel channel;
    public static void main(String[] args) throws IOException, TimeoutException{

        Scanner in = new Scanner(System.in);
        UserService userService = new UserService();
        String usuario, senha;
        System.out.print("Digite seu usuário: ");
        usuario = in.nextLine();
        System.out.print("Senha: ");
        senha = in.nextLine();

        ConnectToRabbitMQ();
        try{
            if (userService.autenticate(usuario, senha)){
                    System.out.print("Chat user: ");
                    String usuario2 = in.nextLine();

                    userService.connectToUser(usuario, usuario2);
                    userService.connectToUser(usuario2, usuario);

                boolean autoAck = false;
                String ListenQueueName = usuario2+"-"+usuario;
                String sendQueueName = usuario+"-"+usuario2;
                channel.basicConsume(ListenQueueName, autoAck, "myConsumerTag",
                        new DefaultConsumer(channel) {
                            @Override
                            public void handleDelivery(String consumerTag,
                                                       Envelope envelope,
                                                       AMQP.BasicProperties properties,
                                                       byte[] body)
                                    throws IOException
                            {
                                String routingKey = envelope.getRoutingKey();
                                String contentType = properties.getContentType();
                                long deliveryTag = envelope.getDeliveryTag();

                                System.out.println(new String(body));

                                channel.basicAck(deliveryTag, false);
                            }
                        });
                System.out.println("Você entrou na sala com "+usuario2);
                userService.sendMessage(usuario+" entrou na sala", sendQueueName);
                while (true) {
                    String message = in.nextLine();
                    if (message.equals("/quit")){
                        System.out.println("Saindo");
                        userService.sendMessage(usuario+" saiu da sala", sendQueueName);
                        System.exit(0);
                    }
                    userService.sendMessage("["+usuario+"]:"+message, sendQueueName);
                }
            } else{
                System.out.print("Autenticação falhou");
                System.exit(0);
            }

        } catch (Exception e){
            System.out.print(e.getMessage());
        }


    }

    private static void ConnectToRabbitMQ() throws TimeoutException, IOException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername("guest");
        factory.setPassword("guest");
        factory.setHost("localhost");
        factory.setPort(5672);

        conn = factory.newConnection();
        channel = conn.createChannel();
    }
}