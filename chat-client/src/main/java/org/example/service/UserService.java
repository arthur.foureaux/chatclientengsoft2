package org.example.service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.hc.client5.http.ClientProtocolException;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.io.entity.StringEntity;
import org.example.User;

import java.io.IOException;

public class UserService {

    public User getUser(String nickname, String password) throws ClientProtocolException, IOException, ParseException {
        User user = null;

        HttpGet request = new HttpGet("http://localhost:8080/login");

        try(CloseableHttpClient httpClient = HttpClientBuilder.create().disableRedirectHandling().build();
            CloseableHttpResponse response = httpClient.execute(request))    {
            HttpEntity entity = response.getEntity();
            if (entity != null){
                String result = EntityUtils.toString(entity);

                Gson gson = new Gson();
                user = gson.fromJson(result, User.class);
            }

        }
        return user;
    }

    public Boolean autenticate(String nickname, String password) throws ClientProtocolException, IOException, ParseException{
        HttpPost request = new HttpPost("http://localhost:8080/login");
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("nickname",nickname);
        jsonObject.addProperty("password",password);

        String json = jsonObject.toString();

        StringEntity entity = new StringEntity(json);
        request.setEntity(entity);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");

        try(CloseableHttpClient httpClient = HttpClientBuilder.create().disableRedirectHandling().build();
            CloseableHttpResponse response = httpClient.execute(request))    {

            if (response.getCode() == 200){
                return true;
            } else {
                return false;
            }

        }
    }

    public Boolean connectToUser(String user1, String user2) throws ClientProtocolException, IOException, ParseException{

        String queueName = user1+"-"+user2;
        HttpPost request = new HttpPost("http://localhost:8080/queue/"+queueName);

        request.setEntity(new StringEntity(queueName));
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");

        try(CloseableHttpClient httpClient = HttpClientBuilder.create().disableRedirectHandling().build();
            CloseableHttpResponse response = httpClient.execute(request))    {

            if (response.getCode() == 200){
                return true;
            } else {
                return false;
            }

        }
    }


    public boolean sendMessage(String message, String queueName) throws ClientProtocolException, IOException, ParseException{

        HttpPost request = new HttpPost("http://localhost:8080/queue/send");
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message",message);
        jsonObject.addProperty("queueName",queueName);

        String json = jsonObject.toString();

        StringEntity entity = new StringEntity(json);
        request.setEntity(entity);
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");

        try(CloseableHttpClient httpClient = HttpClientBuilder.create().disableRedirectHandling().build();
            CloseableHttpResponse response = httpClient.execute(request))    {

            if (response.getCode() == 200){
                return true;
            } else {
                return false;
            }

        }
    }
}
